import logo from './logo.svg';
import React, { Fragment } from 'react';
import './App.css';
class FirstComponent extends React.Component
{
  render(){
      return(
        <Fragment>
         <nav class="navbar navbar-expand-lg navbar-light">
  <a class="navbar-brand" href="#">Logo</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Contact</a>
      </li>
    </ul>
  </div>
</nav>
         </Fragment>
      );
  }
}
class SecondComponent extends React.Component
{
  render(){
    return(
       <Fragment>
          <section id='banner'>
            <div className='container'>
                <div className='row'>
                  <div className='col-md-6 mt-5'>
                      <h1 className='banner-haead-1'>Reactjs Banner Section Components</h1>
                      <h3 className='banner-haead-2'>It is a Libraray To The UI-Components</h3>
                  </div>
                  <div className='col-md-6'>
                  <img src={logo} className="App-logo" alt="logo" />
                  </div>
                </div>
            </div>

          </section>

       </Fragment>
    );
  }
}

class ThirdComponent extends React.Component
{
  render(){
    return(
      <h1>ThirdComponent</h1>
    );
  }
}
function FourthComponent()
{
  return(
      <h1>Function Based FourthComponent</h1>
  );
}

function App() {
  return (
    <div className="App">
       <FirstComponent></FirstComponent>
       <SecondComponent></SecondComponent>
       <ThirdComponent></ThirdComponent>
       <FourthComponent></FourthComponent>
    </div>
  );
}

export default App;
