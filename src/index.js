import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
// import Props from './Props';
import './header.css';
import reportWebVitals from './reportWebVitals';
// import PropsComponent from './Props';

const  root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
    {/* <PropsComponent/> <PropsComponent name="sathish" empid={1001}/>*/}
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
